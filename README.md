
## Prolixer
Abstract logging object designed to give the console.* output-buffer a signature style

**Kind**: global class  

* [Prolixer](#markdown-header-prolixer)
    * [new Prolixer()](#markdown-header-new-prolixer)
    * _instance_
        * [.style](#markdown-header-prolixerstyle-string) ⇒ string
        * [.config](#markdown-header-prolixerconfig-prolixerconfig) ⇒ ProlixerConfig
        * [.info](#markdown-header-prolixerinfo-) ⇒ *
        * [.log](#markdown-header-prolixerlog-) ⇒ *
        * [.debug](#markdown-header-prolixerdebug-) ⇒ *
        * [.trace](#markdown-header-prolixertrace-) ⇒ *
        * [.warn](#markdown-header-prolixerwarn-) ⇒ *
        * [.error](#markdown-header-prolixererror-) ⇒ *
        * [.ApplyConfig(configSettings)](#markdown-header-prolixerapplyconfigconfigsettings)
    * _static_
        * [.style](#markdown-header-prolixerstyle-string) ⇒ string
        * [.config](#markdown-header-prolixerconfig-prolixerconfig) ⇒ ProlixerConfig
        * [.info](#markdown-header-prolixerinfo-) ⇒ *
        * [.log](#markdown-header-prolixerlog-) ⇒ *
        * [.debug](#markdown-header-prolixerdebug-) ⇒ *
        * [.trace](#markdown-header-prolixertrace-) ⇒ *
        * [.warn](#markdown-header-prolixerwarn-) ⇒ *
        * [.error](#markdown-header-prolixererror-) ⇒ *
        * [.getInstance()](#markdown-header-prolixergetinstance-prolixer) ⇒ Prolixer

### new Prolixer()
Default constructor for the class

### prolixer.style ⇒ string
Gets the style for the console

**Kind**: instance property of [Prolixer](#markdown-header-new-prolixer)  
### prolixer.config ⇒ ProlixerConfig
Gets the configuration for the class

**Kind**: instance property of [Prolixer](#markdown-header-new-prolixer)  
### prolixer.info ⇒ *
Writes to the console.info, and any other configured adapter if enabled

**Kind**: instance property of [Prolixer](#markdown-header-new-prolixer)  
### prolixer.log ⇒ *
Writes to the console.log, and any other configured adapter if enabled

**Kind**: instance property of [Prolixer](#markdown-header-new-prolixer)  
### prolixer.debug ⇒ *
Writes to the console.debug, and any other configured adapter if enabled

**Kind**: instance property of [Prolixer](#markdown-header-new-prolixer)  
### prolixer.trace ⇒ *
Writes to the console.trace, and any other configured adapter if enabled

**Kind**: instance property of [Prolixer](#markdown-header-new-prolixer)  
### prolixer.warn ⇒ *
Writes to the console.warn, and any other configured adapter if enabled

**Kind**: instance property of [Prolixer](#markdown-header-new-prolixer)  
### prolixer.error ⇒ *
Writes to the console.error, and any other configured adapter if enabled

**Kind**: instance property of [Prolixer](#markdown-header-new-prolixer)  
### prolixer.ApplyConfig(configSettings)
Applies the configuration setting to an already substantiated [Prolixer](#Prolixer) object

**Kind**: instance method of [Prolixer](#markdown-header-new-prolixer)  

| Param | Type |
| --- | --- |
| configSettings | ProlixerConfig | 

### Prolixer.style ⇒ string
Gets the style for the console

**Kind**: static property of [Prolixer](#markdown-header-new-prolixer)  
### Prolixer.config ⇒ ProlixerConfig
Gets the configuration for the class

**Kind**: static property of [Prolixer](#markdown-header-new-prolixer)  
### Prolixer.info ⇒ *
Writes to the console.info, and any other configured adapter if enabled

**Kind**: static property of [Prolixer](#markdown-header-new-prolixer)  
### Prolixer.log ⇒ *
Writes to the console.log, and any other configured adapter if enabled

**Kind**: static property of [Prolixer](#markdown-header-new-prolixer)  
### Prolixer.debug ⇒ *
Writes to the console.debug, and any other configured adapter if enabled

**Kind**: static property of [Prolixer](#markdown-header-new-prolixer)  
### Prolixer.trace ⇒ *
Writes to the console.trace, and any other configured adapters if enabled

**Kind**: static property of [Prolixer](#markdown-header-new-prolixer)  
### Prolixer.warn ⇒ *
Writes to the console.warn, and any other configured adapter if enabled

**Kind**: static property of [Prolixer](#markdown-header-new-prolixer)  
### Prolixer.error ⇒ *
Writes to the console.error, and any other configured adapter if enabled

**Kind**: static property of [Prolixer](#markdown-header-new-prolixer)  
### Prolixer.getInstance() ⇒ Prolixer
Gets the default instance from the  constant

**Kind**: static method of [Prolixer](#markdown-header-new-prolixer)  
## ProlixerLevelType
Log level object structure

**Kind**: global class  

* [ProlixerLevelType](#markdown-header-prolixerleveltype)
    * [new ProlixerLevelType(defaultStatus)](#markdown-header-new-prolixerleveltypedefaultstatus)
    * [.isEnabled](#markdown-header-prolixerleveltypeisenabled-boolean) ⇒ boolean
    * [.enable()](#markdown-header-prolixerleveltypeenable)
    * [.disable()](#markdown-header-prolixerleveltypedisable)

### new ProlixerLevelType(defaultStatus)
Default constructor


| Param | Type |
| --- | --- |
| defaultStatus | boolean | 

### prolixerLevelType.isEnabled ⇒ boolean
Gets the log level status

**Kind**: instance property of [ProlixerLevelType](#markdown-header-new-prolixerleveltypedefaultstatus)  
### prolixerLevelType.enable()
Enables the log type

**Kind**: instance method of [ProlixerLevelType](#markdown-header-new-prolixerleveltypedefaultstatus)  
### prolixerLevelType.disable()
Disables the log type

**Kind**: instance method of [ProlixerLevelType](#markdown-header-new-prolixerleveltypedefaultstatus)  
## ProlixerConfig
The Prolixer configuration object

**Kind**: global class  

* [ProlixerConfig](#markdown-header-prolixerconfig)
    * [new ProlixerConfig()](#markdown-header-new-prolixerconfig)
    * [.log_levels](#markdown-header-prolixerconfiglog_levels-object) ⇒ Object ⎮ *
    * [.application_name](#markdown-header-prolixerconfigapplication_name-string) ⇒ string
    * [.application_name](#markdown-header-prolixerconfigapplication_name)
    * [.primary_color](#markdown-header-prolixerconfigprimary_color-string) ⇒ string
    * [.primary_color](#markdown-header-prolixerconfigprimary_color)
    * [.icon_url](#markdown-header-prolixerconfigicon_url-string) ⇒ string
    * [.icon_url](#markdown-header-prolixerconfigicon_url)

### new ProlixerConfig()
Default constructor for the object

### prolixerConfig.log_levels ⇒ Object ⎮ *
Gets the log levels and their status

**Kind**: instance property of [ProlixerConfig](#markdown-header-new-prolixerconfig)  
### prolixerConfig.application_name ⇒ string
Gets the application name to use in the console output

**Kind**: instance property of [ProlixerConfig](#markdown-header-new-prolixerconfig)  
### prolixerConfig.application_name
Sets the application name to use in the console output

**Kind**: instance property of [ProlixerConfig](#markdown-header-new-prolixerconfig)  

| Param | Type |
| --- | --- |
| applicationName | string | 

### prolixerConfig.primary_color ⇒ string
Gets the primary color for the text in the console output

**Kind**: instance property of [ProlixerConfig](#markdown-header-new-prolixerconfig)  
### prolixerConfig.primary_color
Sets the primary color for the text in the console output

**Kind**: instance property of [ProlixerConfig](#markdown-header-new-prolixerconfig)  

| Param | Type |
| --- | --- |
| primaryColor | string | 

### prolixerConfig.icon_url ⇒ string
Gets the icon url for console output iconography

**Kind**: instance property of [ProlixerConfig](#markdown-header-new-prolixerconfig)  
### prolixerConfig.icon_url
Sets the icon url for console output iconography

**Kind**: instance property of [ProlixerConfig](#markdown-header-new-prolixerconfig)  

| Param | Type | Description |
| --- | --- | --- |
| iconUrl | string | Ex: data:image/png;base64,iVBOR... --or-- http(s)://cdn.io/img.png |

## LogTypes : Object
Collection of log types

**Kind**: global constant  
## ProlixerDefaultInstance : [Prolixer](#markdown-header-new-prolixer)
Default Prolixer instance for static use.

**Kind**: global constant  
